package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport {

	protected byte number_of_sails;

	public Ship(String id, byte sails) {
		super(id, 0, 0);
		this.number_of_sails = sails;
		// TODO Auto-generated constructor stub
	}

	public Ship(String id, float consumption, int tankSize) {
		super(id, consumption, tankSize);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String move(Road road) {
		// TODO Auto-generated method stub
		if (!(road instanceof WaterRoad)) {
			return "Cannot sail on " + road.toString();
		}

		return this.getType() + " is sailing on " + road + " with " + this.number_of_sails + " sails";
	}
}
